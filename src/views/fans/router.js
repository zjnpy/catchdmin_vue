export default {
    fans: () => import('@/views/fans/fans'),
    backFollow: () => import('@/views/fans/backFollow'),
    fansMsg: () => import('@/views/fans/fansMsg')
}
